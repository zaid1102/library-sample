<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('u_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->bigInteger('mobile')->unique();
            $table->string('email')->unique();
            $table->addColumn('tinyInteger', 'age', ['length' => 3]);
            $table->enum('gender', ['m', 'f', 'o']);
            $table->string('city');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
