<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Book;
use App\Models\UserBook;
use App\Http\Resources\BookResource;
use App\Http\Resources\UserBookResource;
use Validator;

class BookController extends Controller
{
    public function index() {

        $list = Book::paginate(10);

        return BookResource::collection($list)->additional([
            'response' => [
                'status' => 'success'
            ]
        ]);
    }

    public function show($id)
    {
        //Validation
        $validator = Validator::make(['b_id' => $id], [
            'b_id' => 'numeric|exists:books,b_id',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }
        $book = Book::find($id);
        return  new BookResource($book);
    }

    public function store(Request $request)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'book_name' => 'required|string',
            'author' => 'required|string',
            'cover_image' =>'string',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }

        $insert = [
            'book_name' => ucfirst($request->book_name),
            'author' => ucfirst($request->author),
            'cover_image' => $request->cover_image,
        ];

        //Create Book in Database
        $book = Book::create($insert);

        if ($book) {

            return (new BookResource($book))->additional([
                'response' => [
                    'action' => 'created'
                ]
            ]);
        } else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "Book Model: Internal Server Error"]], Response::HTTP_OK);
        }
    }

    public function update(Request $request)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'b_id' => 'required|numeric|exists:books,b_id',
            'book_name' => 'string',
            'author' => 'string',
            'cover_image' => 'string',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }

        $book = Book::find($request->b_id);
        if (isset($request->book_name)) {
            $book->book_name = ucfirst($request->book_name);
        }
        if (isset($request->author)) {
            $book->author = ucfirst($request->author);
        }
        if (isset($request->cover_image)) {
            $book->cover_image = $request->cover_image;
        }

        //Update Book
        if ($book->save()) {
            return (new BookResource($book))->additional([
                'response' => [
                    'action' => 'updated'
                ]
            ]);
        } else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "Internal Server Error"]], Response::HTTP_OK);
        }
    }

    public function destroy($id)
    {
        $book = Book::find($id);
        if ($book) {
            $book->delete();
            return (new BookResource($book))->additional([
                'response' => [
                    'action' => 'deleted'
                ]
            ]);
        } else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "Resource Not Found"]], Response::HTTP_OK);
        }
    }

    public function book_rent(Request $request) {
        //Validation
        $validator = Validator::make($request->all(), [
            'u_id' => 'required|numeric|exists:users,u_id',
            'b_id' => 'required|numeric|exists:books,b_id',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }

        $insert = [
            'u_id' => $request->u_id,
            'b_id' => $request->b_id,
        ];

        //Create Book Rental in Database
        $user_book = UserBook::create($insert);

        if ($user_book) {

            return (new UserBookResource($user_book))->additional([
                'response' => [
                    'action' => 'created'
                ]
            ]);
        } else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "User Book Model: Internal Server Error"]], Response::HTTP_OK);
        }
    }

    public function book_return(Request $request) {

        //Validation
        $validator = Validator::make($request->all(), [
            'ub_id' => 'required|numeric|exists:user_books,ub_id',
            'u_id' => 'required|numeric|exists:users,u_id',
            'b_id' => 'required|numeric|exists:books,b_id',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }

        $user_book = UserBook::find($request->ub_id);

        if($user_book->is_returned == 0) {
            $user_book->is_returned = 1;

            //Mark Book as Returned in Database
            if($user_book->save()) {
                return (new UserBookResource($user_book))->additional([
                    'response' => [
                        'action' => 'book returned'
                    ]
                ]);
            } else {
                return response()->json(['response' => ['status' => 'error', 'detail' => "Resource Not Found"]], Response::HTTP_OK);
            }
        }
        else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "Book already returned"]], Response::HTTP_OK);
        }

    }

    public function get_user_rental_details() {
        $users = User::get('u_id');

        if(count($users) > 0) {
            foreach($users as $key=>$user) {
                $books_rented = UserBook::select('books.b_id','book_name','author','cover_image')->join('books','books.b_id','user_books.b_id')->where(['u_id' => $user['u_id']])->get();
                if(count($books_rented) > 0) {
                    $user['books_rented'] = $books_rented;
                }
                else {
                    unset($users[$key]);
                }
            }
            return response()->json(['data' =>$users,'response' => ['status' => 'success']], Response::HTTP_OK);
        }
        else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "No data found"]], Response::HTTP_OK);
        }
    }
}
