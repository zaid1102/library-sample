<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Resources\UserResource;
use Validator;

class UserController extends Controller
{
    public function index() {

        $list = User::paginate(10);

        return UserResource::collection($list)->additional([
            'response' => [
                'status' => 'success'
            ]
        ]);
    }

    public function show($id)
    {
        //Validation
        $validator = Validator::make(['u_id' => $id], [
            'u_id' => 'numeric|exists:users,u_id',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }
        $user = User::find($id);
        return  new UserResource($user);
    }

    public function update(Request $request)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'u_id' => 'required|numeric|exists:users,u_id',
            'firstname' => 'string',
            'lastname' => 'string',
            'mobile' => "numeric|digits:10|unique:users,mobile,".$request->u_id.",u_id",
            'email' => "email|unique:users,email,".$request->u_id.",u_id",
            'age' => 'integer|digits_between:1,3',
            'gender' =>'in:m,f,o',
            'city' => 'string|between:2,100',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }

        $user = User::find($request->u_id);
        if (isset($request->firstname)) {
            $user->firstname = ucfirst($request->firstname);
        }
        if (isset($request->lastname)) {
            $user->lastname = ucfirst($request->lastname);
        }
        if (isset($request->mobile)) {
            $user->mobile = $request->mobile;
        }
        if (isset($request->email)) {
            $user->email = $request->email;
        }
        if (isset($request->age)) {
            $user->age = $request->age;
        }
        if (isset($request->gender)) {
            $user->gender = $request->gender;
        }
        if (isset($request->city)) {
            $user->city = $request->city;
        }
        if (isset($request->password)) {
            $user->password = bcrypt($request->password);
        }

        //Update User
        if ($user->save()) {
            return (new UserResource($user))->additional([
                'response' => [
                    'action' => 'updated'
                ]
            ]);
        } else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "Internal Server Error"]], Response::HTTP_OK);
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return (new UserResource($user))->additional([
                'response' => [
                    'action' => 'deleted'
                ]
            ]);
        } else {
            return response()->json(['response' => ['status' => 'error', 'detail' => "Resource Not Found"]], Response::HTTP_OK);
        }
    }
}
