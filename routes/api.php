<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('/register', [AuthController::class, 'register']); /* User Register */
    Route::post('/login', [AuthController::class, 'login']); /* User Login */
});
Route::group(['middleware' => ['jwt.verify'],'prefix' => 'auth'], function ($router) {
    Route::get('/user-profile', [AuthController::class, 'userProfile']); /* Get User Profile */
});

Route::group(['middleware' => ['jwt.verify'],'prefix' => 'user'], function ($router) {
    Route::get('/all', [UserController::class, 'index']); /* Get All Users */
    Route::get('/fetch/{id}', [UserController::class, 'show']); /* Get User by User Id */
    Route::put('/update', [UserController::class, 'update']); /* Update User */
    Route::delete('/delete/{id}', [UserController::class, 'destroy']); /* Delete User */
});

Route::group(['middleware' => ['jwt.verify'],'prefix' => 'book'], function ($router) {
    Route::get('/all', [BookController::class, 'index']); /* Get All Books */
    Route::get('/fetch/{id}', [BookController::class, 'show']); /* Get Book by Id */
    Route::post('/create', [BookController::class, 'store']); /* Store Book */
    Route::put('/update', [BookController::class, 'update']); /* Update Book */
    Route::post('/delete/{id}', [BookController::class, 'destroy']); /* Delete Book */

    Route::post('/rent', [BookController::class, 'book_rent']); /* User Book Rent */
    Route::put('/return', [BookController::class, 'book_return']); /* User Book Return */
    Route::get('/rentals', [BookController::class, 'get_user_rental_details']); /* Get User Rental Records */
});
